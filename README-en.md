# Introduction

This is an example of quickly plugging in `DongTai IAST` in a `SpringBoot` service.

The demo can be done at either **Development Locally** or **Build the Gitlab CI pipeline**.

# Get DongTai IAST Agent download url 
Login [DongTai](https://iast.io/login) or your private deployment DongTai IAST Server.

> Get `iast.io` account: [link](https://jinshuju.net/f/I9PNmf)



![download_agent_en.png](images/download_agent_en.png)


## Download Agent
Download the agent to `/tmp/agent.jar` from the above screenshot link

## Package code

``` 
mvn clean package -Dmaven.test.skip=true
```

## Start service
```
java -javaagent:/tmp/agent.jar -Dproject.name=gitlab-dongtaiiast-demo -jar target/DongTai-IAST-demo*.jar
```

## Request an interface that contains a vulnerability
```
curl localhost:8008/cmd_injection/?cmd=echo
```

# Build the Gitlab CI pipeline

## Get configurations
Get `DONGTAI_SERVER` and `DONGTAI_TOKEN` in the above screenshot.

## Configure variables in CI/CD in Settings
![vars.png](images/vars.png)

## Write build documentation

`.gitlab-ci.yml`

```yaml
stages:
  - build
  - deploy

variables:
  jar_name: DongTai-IAST-demo-0.0.1-SNAPSHOT.jar
  project_name: gitlab-dongtaiiast-demo

maven-build:
  stage: build
  image: maven:3.5.0-jdk-8
  script:
    - mvn package -B -Dmaven.test.skip=true
  cache:
    key: m2-repo
    paths:
      - .m2/repositor
  artifacts:
    paths:
      - target/$jar_name

deploy-app:
  stage: deploy
  image: openjdk:8-jdk-alpine
  before_script:
    - apk update && apk add curl
  script:
    - curl -v -X GET "$DONGTAI_SERVER/openapi/api/v1/agent/download?url=$DONGTAI_SERVER/openapi&language=java" -H "$DONGTAI_TOKEN" -o /tmp/agent.jar -k
    - nohup java -javaagent:/tmp/agent.jar -Dproject.name=$project_name -jar target/DongTai-IAST-demo*.jar > $jar_name.log 2>&1 &
    - sleep 30
    - >
      for i in $(seq 1 3); do
        curl localhost:8008/cmd_injection/?cmd=echo
        sleep 1
      done
    - kill -9 $(jps | grep DongTai-IAST-demo | awk '{print $1}')
    - echo "Finished"
```

> Here, DongTai-IAST-demo-0.0.1-SNAPSHOT.jar is only temporarily activated to access the trigger vulnerability and upload it to DongTai Server, and there is no real deployment.
.


#Check vulnerability

The above two operations will view the vulnerability list in the corresponding DongTai IAST Server application vulnerability function.

![vuls.png](images/vuls_en.png)
