package io.dongtai.iast;

import io.dongtai.iast.controller.VulController;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DongTaiIASTDemoApplicationTests {

    @Test
    public void TestCMDInjection() {
        VulController vul = new VulController();

        String result = vul.commandInjection("echo");
        System.out.println("-----Command Injection in unittest-----");
        Assertions.assertEquals("Command Injection", result);
    }

}
