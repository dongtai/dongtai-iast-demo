package io.dongtai.iast;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@SpringBootApplication
public class DongTaiIASTDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DongTaiIASTDemoApplication.class, args);
    }

}


