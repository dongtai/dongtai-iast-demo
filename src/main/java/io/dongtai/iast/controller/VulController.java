package io.dongtai.iast.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;


@RestController
public class VulController {
    @GetMapping("/cmd_injection")
    @ResponseBody
    public String commandInjection(String cmd) {
        System.out.println("request coming...");
        try {
            Runtime.getRuntime().exec(cmd);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "Command Injection";
    }
}