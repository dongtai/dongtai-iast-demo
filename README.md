# 简介

这是快速在 `SpringBoot` 服务中接入 `DongTai IAST` 的示例。

可以在 **本地部署测试** 或者 **构建 Gitlab CI 流水线，完成测试触发漏洞** 任意一个步骤完成演示。

# 获取 DongTai IAST Agent 下载地址
登录 [DongTai](https://iast.io/login) 或者 私有化部署的DongTai IAST Server。

> 获取 `iast.io` 的账号: [链接](https://jinshuju.net/f/I9PNmf)

![download_agent.png](images/download_agent.png)

# 本地部署测试
## 下载 Agent
通过上述截图链接下载 agent 到 `/tmp/agent.jar`

## 打包项目

``` 
mvn clean package -Dmaven.test.skip=true
```

## 运行服务
```
java -javaagent:/tmp/agent.jar -Dproject.name=gitlab-dongtaiiast-demo -jar target/DongTai-IAST-demo*.jar
```

## 请求包含漏洞的接口
```
curl localhost:8008/cmd_injection/?cmd=echo
```

# 构建 Gitlab CI 流水线，完成测试触发漏洞

## 获取配置
在上述截图的位置获取`DONGTAI_SERVER` 和 `DONGTAI_TOKEN`。

## 在Settings 的 CI/CD 中配置变量
![vars.png](images/vars.png)

## 编写构建文档 `.gitlab-ci.yml`

```yaml
stages:
  - build
  - deploy

variables:
  jar_name: DongTai-IAST-demo-0.0.1-SNAPSHOT.jar
  project_name: gitlab-dongtaiiast-demo

maven-build:
  stage: build
  image: maven:3.5.0-jdk-8
  script:
    - mvn package -B -Dmaven.test.skip=true
  cache:
    key: m2-repo
    paths:
      - .m2/repositor
  artifacts:
    paths:
      - target/$jar_name

deploy-app:
  stage: deploy
  image: openjdk:8-jdk-alpine
  before_script:
    - apk update && apk add curl
  script:
    - curl -v -X GET "$DONGTAI_SERVER/openapi/api/v1/agent/download?url=$DONGTAI_SERVER/openapi&language=java" -H "$DONGTAI_TOKEN" -o /tmp/agent.jar -k
    - nohup java -javaagent:/tmp/agent.jar -Dproject.name=$project_name -jar target/DongTai-IAST-demo*.jar > $jar_name.log 2>&1 &
    - sleep 30
    - >
      for i in $(seq 1 3); do
        curl localhost:8008/cmd_injection/?cmd=echo
        sleep 1
      done
    - kill -9 $(jps | grep DongTai-IAST-demo | awk '{print $1}')
    - echo "Finished"
```

> 这里只是临时启动 DongTai-IAST-demo-0.0.1-SNAPSHOT.jar 用于访问触发漏洞上传到DongTai Server,并没有真实部署。


#漏洞查看

上述两种操作都会在对应的DongTai IAST Server 的应用漏洞功能查看到漏洞列表。

![vuls.png](images/vuls.png)